import React, { useState } from 'react';
import { Container } from '@material-ui/core';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';

import Navbar from './components/Navbar/Navbar';
import Home from './components/Home/Home';
import Auth from './components/Auth/Auth';
import Account from './components/Account/Account';
import Form from './components/Form/Form';
import Editor from './components/Editor/Editor';
import PostDetails from './components/PostDetails/PostDetails';

const App = () => {

  const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));
  const [currentId, setCurrentId] = useState(null);

// route structure for each pacges access, if user is logged in, will be able to see all pages; but if not, will redirect users to auth pages if they enter the links manually
  return (
    <BrowserRouter>
      <Container maxWidth="xl">
        <Navbar />
        {user ? (
          <Routes>
            <Route path="/" exact element={<Navigate to="/posts" />} />
            <Route path="/posts" element={<Home />} />
            <Route path="/posts/:id" exact element={<PostDetails />} />
            <Route path="/posts/search" element={<Home />} />
            <Route path="/auth" element={<Auth />} />
            <Route path="/account" element={<Account />} />
            <Route path="/account/search" element={<Account />} />
            <Route path="/create" element={<Editor />} />
            <Route path="/upload" element={<Form />} />
          </Routes>
        ) : (
          <Routes>
            <Route path="/" exact element={<Navigate to="/posts" />} />
            <Route path="/posts" element={<Home />} />
            <Route path="/posts/:id" exact element={<PostDetails />} />
            <Route path="/posts/search" element={<Home />} />
            <Route path="/auth" element={<Auth />} />
            <Route path="/account" element={<Navigate to="/auth" />} />
            <Route path="/account/search" element={<Navigate to="/auth" />} />
            <Route path="/create" element={<Navigate to="/auth" />} />
            <Route path="/upload" element={<Navigate to="/auth" />} />
          </Routes>
        )}
      </Container>
    </BrowserRouter>
  )
};

export default App;