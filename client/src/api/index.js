import axios from 'axios';

//link to server side
const API = axios.create({baseURL: 'http://localhost:5000'});

API.interceptors.request.use((req) => {
    if(localStorage.getItem('profile')) {
        req.headers.Authorization = `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`;
    }

    return req;
});

//for getting posts for generic pages
export const fetchPost = (id) => API.get(`/posts/${id}`);
export const fetchPosts = () => API.get(`/posts`);
//searchquery entered will be passed to server side as variable, must declare here
export const fetchPostsBySearch = (searchQuery) => API.get(`/posts/search?searchQuery=${searchQuery.search || 'none'}&filter=${searchQuery.filter}&sort=${searchQuery.sort}&sortnum=${searchQuery.sortnum}`);

//for editing posts for various purposes
export const createPost = (newPost) => API.post('/posts', newPost);
export const likePost = (id) => API.patch(`/posts/${id}/likePost`);
export const comment = (value, id) => API.post(`/posts/${id}/commentPost`, { value });
export const updatePost = (id, updatedPost) => API.patch(`/posts/${id}`, updatedPost);
export const deletePost = (id) => API.delete(`/posts/${id}`);

//for account related pages
export const fetchaccountPosts = () => API.get('/account');
export const deleteaccountPost = (id) => API.delete(`/account/${id}`); 
//searchquery entered will be passed to server side as variable, must declare here
export const fetchaccountPostsBySearch = (searchQuery) => API.get(`/account/search?searchQuery=${searchQuery.search || 'none'}&filter=${searchQuery.filter}&sort=${searchQuery.sort}&sortnum=${searchQuery.sortnum}`);

//for account login and signup
export const signIn = (formData) => API.post('/user/signin', formData);
export const signUp = (formData) => API.post('/user/signup', formData);