import React, { useState, useEffect } from 'react';
import { Container, Grow, Grid } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';

import { getaccountPosts } from '../../actions/posts.js';
import AccountPosts from '../AccountPosts/AccountPosts';
import FormID from '../FormID/FormID';
import useStyles from './styles';
import "./style.scss";
import SidebarAccount from '../SidebarAccount/Sidebar';



function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const Account = (post) => {
  const classes = useStyles();
  const query = useQuery();
  const page = query.get('page') || 1;
  const searchQuery = query.get('searchQuery');

    const [currentId, setCurrentId] = useState(null);
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));
    const dispatch = useDispatch();
    const navigate = useNavigate();

    //for generic page loading and reset filter request to initial state
    useEffect(() => {
      sessionStorage.setItem("alikesr", "Greater than or equal to");
      sessionStorage.setItem("alikesnumber", 0);
      dispatch(getaccountPosts());
     }, [currentId,dispatch]);

    //only account page can edit their previous posts
    return(
      <Grow in>
      <Container maxWidth="xl" >

          <Grid>
              {/* Sidebar Filter */}
              <SidebarAccount />

          {/* Posts*/}
          <Grid>
            <AccountPosts setCurrentId={setCurrentId} />
          </Grid>


          {(user?.result?.googleId === post?.creator || user?.result?._id === post?.creator) && (currentId) && (
            <Grid >
              <FormID currentId={currentId} setCurrentId={setCurrentId} />
            </Grid>)
          }
        </Grid>
      </Container>
    </Grow>
  )
};

export default Account;