import React, { useState } from 'react';
import { Card, CardActions, CardContent, CardMedia, Button, Typography, ButtonBase } from '@material-ui/core/';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import DeleteIcon from '@material-ui/icons/Delete';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ThumbUpAltOutlined from '@material-ui/icons/ThumbUpAltOutlined';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { useNavigate } from 'react-router-dom';

import { likePost, deletePost } from '../../../actions/posts';
import './style.scss';
import CommentSection from '../../PostDetails/CommentSection';


const Post = ({ post, setCurrentId }) => {
  const [likes, setLikes] = useState(post?.likes);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));

  const userId = user?.result.googleId || user?.result?._id;
  const hasLikedPost = post?.likes?.find((like) => like === userId);

  const openPost = () => {
    navigate(`/posts/${post._id}`);
  };

  //for filtering use based on likes counts and get info from sidebar search function
  const alikesr = sessionStorage.getItem("alikesr");
  const alikesnumber = sessionStorage.getItem("alikesnumber");
  let alikesrequest = useState (true);

  if (alikesr == "Equals") {
    alikesrequest =  likes.length==alikesnumber;
  }else if (alikesr == "Greater than") {
    alikesrequest =  likes.length>alikesnumber;
  } else if (alikesr == "Less than") {
    alikesrequest =  likes.length<alikesnumber;
  } else if (alikesr == "Less than or equal to") {
    alikesrequest =  likes.length<=alikesnumber;
  } else (
    alikesrequest = likes.length>=alikesnumber
  );

  const handleLike = async () => {
    dispatch(likePost(post._id));

    if (hasLikedPost) {
      setLikes(post.likes.filter((id) => id !== userId));
    } else {
      setLikes([...post.likes, userId]);
    }
  };

  const Likes = () => {
    if (likes.length > 0) {
      return likes.find((like) => like === userId)
        ? (
          <><ThumbUpAltIcon fontSize="small" />&nbsp;{likes.length > 2 ? `You and ${likes.length - 1} others` : `${likes.length} like${likes.length > 1 ? 's' : ''}`}</>
        ) : (
          <><ThumbUpAltOutlined fontSize="small" />&nbsp;{likes.length} {likes.length === 1 ? 'Like' : 'Likes'}</>
        );
    }
    return <><ThumbUpAltOutlined fontSize="small" />&nbsp;Like</>;
  };


  class BlogCard extends React.Component {
    constructor(props) {
      super(props);
      this.state = { flipped: false };
      this.flip = this.flip.bind(this);
    }

    flip = () => {
      this.setState({ flipped: !this.state.flipped });
    }
    render() {
      return (
        <div onMouseEnter={this.flip} onMouseLeave={this.flip} className={"card-container" + (this.state.flipped ? " flipped" : "")}>
          <Front />
          <Back />
        </div>
      )
    }
  }




  class Front extends React.Component {
    render() {
      return (
        <div className="front">
          <div class="blog-slider">
            <div class="blog-slider__wrp swiper-wrapper">
              <div class="blog-slider__item swiper-slide">

                {/* Image */}
                <div class="openPost" onClick={openPost}>
                  <div class="blog-slider__img">
                    <img src={post.selectedFile} alt="Meme Picture" />
                  </div>
                </div>

                 {/* Content */}
                 <div class="blog-slider__content">
                  <span class="blog-slider__code">{moment(post.createdAt).fromNow()}</span>
                  <div class="blog-slider__title">Title: {post.title}</div>
                  <span class="blog-slider__author">Author: {post.creatorname}</span>
                  <div class="blog-slider__text">{post.description}</div>
                </div>

              </div>
            </div>

          </div>
        </div>
      )
    }
  }

  class Back extends React.Component {
    render() {
      return (
        <div className="back">
          <div class="frame">
            <button onClick={openPost} class="custom-btn btn-3"><span>Have a look!</span></button>
              <button
                onClick={(e) => {
                  e.stopPropagation();
                  setCurrentId(post._id);
                }}
                style={{ color: 'white' }}
                size="small"
              >
                <MoreHorizIcon fontSize="default" />
              </button>
              
          </div>
          <CommentSection post={post} />
        </div>
      )
    }
  }

    //only display post that fit the likes count filter request
  return (
    (alikesrequest)?(
      <div className="page-container">
        <div ><BlogCard /></div>
      </div>
      ):(<div></div>)
    );
};
export default Post;




