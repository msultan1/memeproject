import React, {useState, Fragment} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Button, Container, Grow, Grid} from '@material-ui/core';
import ImageEditor from "@toast-ui/react-image-editor"; //tui library used which is licensed for use (license https://github.com/nhn/tui.editor/blob/master/LICENSE
import theme from './styles';
import Form from '../Form/Form';
import './style.scss';

// replace url with image from upload
var imge = 'https://picsum.photos/200';

const Editor = () => {
  const user = JSON.parse(localStorage.getItem('profile'));
  const download = require("downloadjs");
  const navigate = useNavigate();
  const imageEditor = React.createRef();
  // const classes = useStyles();


  const saveImageToComputer = () =>  {
    const imageEditorInst = imageEditor.current.imageEditorInst;
    const data = imageEditorInst.toDataURL();
    download(data);
  }

  const saveImageToDisk = () =>  {
    const imageEditorInst = imageEditor.current.imageEditorInst;
    const data = imageEditorInst.toDataURL();
    if (data) {
      const mimeType = data.split(";")[0];
      const extension = data.split(";")[0].split("/")[1];
      download(data, `image.${extension}`, mimeType);
      sessionStorage.setItem("meme", data);
      navigate('/upload');
  }
};

  return (
    <Grow in>
    <Container >
      <div className="margin"></div>

      <h1> Hi {user.result.name}, have fun creating your meme :) You may choose to upload later and share the fun with others! </h1>

      <Fragment >
      <Button className='button' onClick={saveImageToDisk} variant="contained" color="primary">Save Image and Upload MEME to share the JOY!</Button>
      <Button className='button' onClick={saveImageToComputer} variant="contained" color="primary">Save Image to computer</Button>
      <ImageEditor
        includeUI={{
          loadImage: {
            //path: "img/sampleImage.jpg",
            path: imge,
            name: "SampleImage",
          },
          theme,
          menu: ["shape", "filter", "text", 'mask', 'icon', 'draw', 'crop', 'flip', 'rotate'],
          initMenu: "filter",
          uiSize: {
            width: "1000px",
            height: "700px",
          },
          menuBarPosition: "bottom",
        }}
        cssMaxHeight={500}
        cssMaxWidth={700}
        selectionStyle={{
          cornerSize: 20,
          rotatingPointOffset: 70,
        }}
        usageStatistics={true}
        ref={imageEditor}    />
      </Fragment>
    </Container>
    </Grow>
  
  )
};

export default Editor;

