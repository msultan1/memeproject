import e from "@toast-ui/react-image-editor";
import "tui-image-editor/dist/tui-image-editor.css";



const theme = ({

    "common.bi.image":
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Gruen-logo_lmu2.svg/2000px-Gruen-logo_lmu2.svg.png",
    "common.bisize.width": "50px",
    "common.bisize.height": "50px",
    "common.backgroundImage": "none",
    "common.backgroundColor": "white",
    "common.border": "0px",
  
    // header
    "header.backgroundImage": "none",
    "header.backgroundColor": "white",
    "header.border": "5px",
  
    // load button
    "loadButton.backgroundColor": "#00883A",
    "loadButton.border": "1px solid #ddd",
    "loadButton.color": "white",
    "loadButton.fontFamily": "Roboto, sans-serif",
    "loadButton.fontSize": "12px",
  
    // download button
    // "downloadButton.backgroundColor": "white",
    // "downloadButton.border": "1px solid white",
    // "downloadButton.color": "#white",
    // "downloadButton.fontFamily": "NotoSans, sans-serif",
    // "downloadButton.fontSize": "12px",
    "downloadButton.opacity": "0",
  
    // icons default
    "menu.normalIcon.color": "#8a8a8a",
    "menu.activeIcon.color": "#555555",
    "menu.disabledIcon.color": "#434343",
    "menu.hoverIcon.color": "#e9e9e9",
    "submenu.normalIcon.color": "#8a8a8a",
    "submenu.activeIcon.color": "#e9e9e9",
  
    "menu.iconSize.width": "24px",
    "menu.iconSize.height": "24px",
    "submenu.iconSize.width": "32px",
    "submenu.iconSize.height": "32px",
  
    // submenu primary color
    "submenu.backgroundColor": "#1e1e1e",
    "submenu.partition.color": "#858585",
  
    // submenu labels
    "submenu.normalLabel.color": "#858585",
    "submenu.normalLabel.fontWeight": "lighter",
    "submenu.activeLabel.color": "#fff",
    "submenu.activeLabel.fontWeight": "lighter",
  
    // checkbox style
    "checkbox.border": "1px solid #ccc",
    "checkbox.backgroundColor": "#fff",
  
    // rango style
    "range.pointer.color": "#fff",
    "range.bar.color": "#666",
    "range.subbar.color": "#d1d1d1",
  
    "range.disabledPointer.color": "#414141",
    "range.disabledBar.color": "#282828",
    "range.disabledSubbar.color": "#414141",
  
    "range.value.color": "#fff",
    "range.value.fontWeight": "lighter",
    "range.value.fontSize": "11px",
    "range.value.border": "1px solid #353535",
    "range.value.backgroundColor": "#white",
    "range.title.color": "#fff",
    "range.title.fontWeight": "lighter",
  
    // colorpicker style
    "colorpicker.button.border": "1px solid #1e1e1e",
    "colorpicker.title.color": "#fff",
  });
  
  export default theme;
