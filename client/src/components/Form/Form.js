import React, { useState, useEffect } from 'react';
import { TextField, Button, Typography, Paper, Checkbox, FormControlLabel } from '@material-ui/core';
import {useNavigate} from 'react-router-dom';
import FileBase from 'react-file-base64';
import { useDispatch,useSelector } from 'react-redux';
import useStyles from './styles';
import { createPost, updatePost, deletePost } from '../../actions/posts';

const Form = ({currentId, setCurrentId}) => {
  //bringing meme from editor to form page
  const newmeme = sessionStorage.getItem("meme");

  const [postData, setPostData] = useState({ title: '',description: '', selectedFile: '',isDraft:'true',isPrivate:'false'});
  const post = useSelector((state) => currentId? state.posts.posts.find((post) => post._id===currentId): null);
  const user = JSON.parse(localStorage.getItem('profile'));
  const dispatch = useDispatch();
  const classes = useStyles();
  const navigate = useNavigate();

  //for both check boxes
  const [isprivate, setPrivate] = useState(false);
  const [draft, setDraft] = useState(true);

  useEffect(() => {
    if (post) setPostData(post);
  }, [post]);
  

  //get data from database
  useEffect(() => {
    if(post) setPostData(post);
    if(post) setPrivate(post.isPrivate);
    if(post) setDraft(post.isDraft);
  }, [post]);

  const handlePrivate = (e) => {
    setPrivate(e.target.checked);
  };

  const handleDraft = (e) => {
    setDraft(e.target.checked);
  };

  const handleSubmit = async (e) => {
      e.preventDefault();

      if (currentId){
        dispatch(updatePost(currentId,{...postData,isDraft:draft,isPrivate:isprivate}, navigate));
        navigate('/account');
        clear ();
      } else{
          if (newmeme){
            dispatch(createPost({...postData, selectedFile:newmeme, creatorname:user?.result?.name,isDraft:draft,isPrivate:isprivate}));
          } else {
          dispatch(createPost({...postData, creatorname:user?.result?.name,isDraft:draft,isPrivate:isprivate}));
          }
        navigate('/account');
        clear ();
      }
    };
  
  const  clear = () => {
      setPostData({ title: '',description: '', selectedFile: '',isDraft:'true',isPrivate:'false'});
      setPrivate(false);
      setDraft(true);
      sessionStorage.setItem("meme", '');
      setCurrentId(null);
    }

    const handleDelete = async (e) => {
      e.preventDefault();
      dispatch(deletePost(post._id));
      clear();
      navigate('/account');
    };
    
//form page 
  return (
    <Paper className={classes.paper}>
      <form autoComplete="off" noValidate className={`${classes.root} ${classes.form}`} onSubmit={handleSubmit}>
        <Typography variant="h6" > {currentId ? `Editing "${post?.title}"` : 'Upload your MEME with us!'}</Typography>
        <TextField name="title" variant="outlined" label="Meme Title" fullWidth value={postData.title} onChange={(e) => setPostData({ ...postData, title: e.target.value })} />
        <TextField name="description" variant="outlined" label="Meme Description" fullWidth value={postData.description} onChange={(e) => setPostData({ ...postData, description: e.target.value })} />
        {newmeme ? 
          (<Typography>Meme ready to be shared</Typography>):
          (<div className={classes.fileInput}><FileBase type="file" multiple={false} onDone={({ base64 }) => setPostData({ ...postData, selectedFile: base64 })} /></div>)
        }
        <FormControlLabel control={<Checkbox checked={isprivate} onChange={handlePrivate} />} label="Private" />
        <FormControlLabel control={<Checkbox checked={draft} onChange={handleDraft} />} label="Save as Draft" />
        <Button className={classes.buttonSubmit} variant="contained" color="primary" size="large" type="submit" fullWidth>Submit</Button>
        {newmeme ? 
          (<Button className={classes.buttonClear}variant="contained" color="secondary" size="large" onClick={clear} fullWidth>Clear - Are you sure to clear your MEME as well?</Button>):
          (<Button className={classes.buttonClear}variant="contained" color="secondary" size="large" onClick={clear} fullWidth>Clear</Button>)
        }
        <div></div>
        {currentId ? 
          (<Button className={classes.buttonDelete}variant="contained" color="secondary" size="small" onClick={handleDelete} fullWidth>Delete your post</Button>):(<div></div>)
        }
      </form>
    </Paper>

    
  );
}

export default Form;