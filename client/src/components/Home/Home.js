import React, { useState, useEffect } from 'react';
import { Container, Grow, Grid } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';

import { getPosts } from '../../actions/posts.js';
import Posts from '../Posts/Posts';
import Form from '../Form/Form';
import useStyles from './styles';
import "./style.scss";
import Sidebar from '../Sidebar/Sidebar';


function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const Home = (post) => {

    const classes = useStyles();
    const query = useQuery();
    const page = query.get('page');
    const searchQuery = query.get('searchQuery');

    const [currentId, setCurrentId] = useState(null);
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));
    const dispatch = useDispatch();
    const navigate = useNavigate();

    //for generic page loading
    useEffect(() => {
        sessionStorage.setItem("likesr", "Greater than or equal to");
        sessionStorage.setItem("likesnumber", 0);
        dispatch(getPosts());
    }, [currentId, dispatch]);

    return (
        <Grow in>
            <Container maxWidth="xl" >

                <Grid>
                    {/* Sidebar Filter */}
                    <Sidebar />

                    {/* Posts*/}
                    <Grid >
                        <Posts setCurrentId={setCurrentId} />
                    </Grid>


                    {(user?.result?.googleId === post?.creator || user?.result?._id === post?.creator) && (currentId) && (
                        <Grid >
                            <Form currentId={currentId} setCurrentId={setCurrentId} />
                        </Grid>)
                    }
                    
                </Grid>
            </Container>
        </Grow>
    )
};

export default Home;
