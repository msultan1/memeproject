import React, { useState, useRef } from "react";
import { Typography, TextField, Button } from "@material-ui/core/";
import ThumbUpAltIcon from "@material-ui/icons/ThumbUpAlt";
import ThumbUpAltOutlined from "@material-ui/icons/ThumbUpAltOutlined";
import { useDispatch } from "react-redux";
import { likePost, deletePost, commentPost } from "../../actions/posts";
import useStyles from "./styles";
import "./commStyle.scss";

const CommentSection = ({ post }) => {
  const user = JSON.parse(localStorage.getItem("profile"));
  const [comment, setComment] = useState("");
  const dispatch = useDispatch();
  const [comments, setComments] = useState(post?.comments);
  const classes = useStyles();
  const commentsRef = useRef();

  const userId = user?.result.googleId || user?.result?._id;
  const [likes, setLikes] = useState(post?.likes);
  const hasLikedPost = post?.likes?.find((like) => like === userId);

  const handleComment = async () => {
    const newComments = await dispatch(
      commentPost(`${user?.result?.name}: ${comment}`, post._id)
    );

    setComment("");
    setComments(newComments);

    commentsRef.current.scrollIntoView({ behavior: "smooth" });
  };

  const handleLike = async () => {
    dispatch(likePost(post._id));

    if (hasLikedPost) {
      setLikes(post.likes.filter((id) => id !== userId));
    } else {
      setLikes([...post.likes, userId]);
    }
  };

  const Likes = () => {
    if (likes.length > 0) {
      return likes.find((like) => like === userId) ? (
        <>
          <ThumbUpAltIcon fontSize="small" />
          &nbsp;
          {likes.length > 2
            ? `You and ${likes.length - 1} others`
            : `${likes.length} like${likes.length > 1 ? "s" : ""}`}
        </>
      ) : (
        <>
          <ThumbUpAltOutlined fontSize="small" />
          &nbsp;{likes.length} {likes.length === 1 ? "Like" : "Likes"}
        </>
      );
    }
    return (
      <>
        <ThumbUpAltOutlined fontSize="small" />
        &nbsp;Like
      </>
    );
  };

  return (
    <div className="commentBox">
      <div className="post">
        <div className="postBody">
          <div className="postContent"></div>
        </div>

        {/* Create a Comment */}
        <form class="createComment">
          <label htmlFor="comment">Your Comment</label>
          <textarea
            id="comment"
            type="text"
            placeholder="Comment"
            value={comment}
            onChange={(e) => setComment(e.target.value)}
            required
          />
        <div class="directionComment like-content">
          <button class="sepComment" onClick={handleComment} type="submit">
            Post Comment
          </button>

          <button
            class="sepComment btn-secondary like-review"
            disabled={!user?.result}
            onClick={handleLike}
          >
            <Likes />
          </button>
          </div>
          
        </form>

        {/* <div class="like-content">
          <button
            class="btn-secondary like-review"
            disabled={!user?.result}
            onClick={handleLike}
          >
            <Likes />
          </button>
        </div> */}
      </div>

      {/* History of comments */}

      <div className="comment">
        <div className="commentBody">
          <span className="commentContent">
            {comments?.map((c, i) => (
              <p color="white" key={i} gutterBottom variant="subtitle1">
                <h3 className="commentAuthor">{c.split(": ")[0]}</h3>
                {c.split(":")[1]}
              </p>
            ))}
          </span>
        </div>
      </div>
    </div>
  );
};

export default CommentSection;
