import React, { useEffect } from "react";
import {
  Paper,
  Typography,
  CircularProgress,
  Divider,
  Container,
  ButtonBase,
} from "@material-ui/core/";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { useParams, useNavigate } from "react-router-dom";
import { getPost } from "../../actions/posts";
import useStyles from "./styles";
import CommentSection from "./CommentSection";
import "./postStyle.scss";

const PostDetails = () => {
  const { post, posts, isLoading } = useSelector((state) => state.posts);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const classes = useStyles();
  const { id } = useParams();

  useEffect(() => {
    dispatch(getPost(id));
  }, [id]);

  if (!post) return null;

  return (
    <main class="PDprofile">
      {/* <div class="PDprofile-bg"></div>
      <section class="PDcontainer">
        <aside class="PDprofile-image">
          <a class="PDcamera">
            <i class="fas fa-camera"></i>
          </a>
          <img class="PDprof-image" src={post.selectedFile} />
        </aside>
        <section class="PDprofile-info">
          <h1 class="PDfirst-name">{post.creatorname}</h1>
          <h1 class="PDsecond-name">{post.title}</h1>
          <h2>ABOUT</h2>
          <p>{post.description}</p>
          <aside class="PDsocial-media-icons">
            <a href="https://twitter.com/intent/tweet" target="_blank">
              <i class="fab fa-twitter"></i>
            </a>
            <a
              href="https://www.facebook.com/sharer/sharer.php?u=localhost:3000/posts"
              target="_blank"
            >
              <i class="fab fa-facebook"></i>
            </a>
          </aside>
        </section>
      </section>
      <section class="PDstatistics">
        <div class="info">
          <CommentSection post={post} />
        </div>
      </section>     */}
    </main>
  );
};

export default PostDetails;

// <Container>
//   <section class="showcase">
//     <div class="img">
//       <img class="media" src={post.selectedFile} />
//     </div>
//   </section>
//   <section class="showcase">
//     <div class="info">
//       <h4 class="logo">{post.title}</h4>
//       <h3>Creator Name: {post.creatorname}</h3>
//       <i><b> Created:</b> {moment(post.createdAt).fromNow()}</i>
//       <p><b>Description:</b> {post.description}</p>
//     </div>
//   </section>
//   <section class="showcase">
//     <div class="info">
//       <CommentSection post={post} />
//     </div>
//   </section>
//   <section ></section>
// </Container>
