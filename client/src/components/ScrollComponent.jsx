import React, { Component } from "react";
import axios from "axios";
import Posts from '../components/Posts/Posts';
import Post from '../components/Posts/Post/Post';

class ScrollComponent extends Component {
  constructor() {
    super();
    this.state = {
      posts: [],
      loading: false,
      page: 0,
      prevY: 0,
    };
  }

  fetchPosts(page) {
    this.setState({ loading: true });
    axios
      .get(
        `https://jsonplaceholder.typicode.com/photos?_page=${page}&_limit=10`
      )
      .then((res) => {
        this.setState({
          posts: [...this.state.posts, ...res.data],
        });
        this.setState({ loading: false });
      });
  }

  componentDidMount() {
    this.fetchPosts(this.state.page);

    var options = {
      root: null,
      rootMargin: "0px",
      threshold: 1.0,
    };

    this.observer = new IntersectionObserver(
      this.handleObserver.bind(this),
      options
    );
    this.observer.observe(this.loadingRef);
  }

  handleObserver(entities, observer) {
    const y = entities[0].boundingClientRect.y;
    if (this.state.prevY > y) {
      const posts = this.state.posts[this.state.posts.length - 1];
      const curPage = posts.id;
      this.fetchPosts(curPage);
      this.setState({ page: curPage });
    }
    this.setState({ prevY: y });
  }

  render() {
    // Additional css
    const loadingCSS = {
      height: "100px",
      margin: "50px",
    };

    // To change the loading icon behavior
    const loadingTextCSS = { display: this.state.loading ? "block" : "none" };

  

    return (
      <div className="container">
        <div style={{ minHeight: "300px" }}>
          {this.state.posts.map((user) => (
            <posts src={user.url} height="100px" width="200px" />
          ))}
        </div>
        <div
          ref={(loadingRef) => (this.loadingRef = loadingRef)}
          style={loadingCSS}
        >
          <span style={loadingTextCSS}>Loading...</span>
        </div>
      </div>
    );
  }
}

export default ScrollComponent;
