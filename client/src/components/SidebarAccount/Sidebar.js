import React, { useState, useEffect } from 'react';
import { Paper, Button, TextField, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import { getaccountPosts, getaccountPostsBySearch } from '../../actions/posts.js';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import '../Sidebar/sidStyle.scss';
import useStyles from './styles';


const Sidebar = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const navigate = useNavigate();


  //search, filter, sort
  const [search, setSearch] = useState('');
  const [filteritem, setFilteritem] = React.useState(["All", "Title", "Description", "Tags", "Comments"]);
  const [filter, setFilter] = useState("All");
  let [alikesnumber, setaLikesnumber] = useState('');
  const [alikesrange, setaLikerange] = React.useState(["Greater than", "Less than","Greater than or equal to","Less than or equal to", "Equals"]);
  const [alikesr, setaLikesr] = useState("Greater than or equal to");
  const [sortseq, setSortseq] = React.useState(["DESC - Z-A/ Latest", "ASC - A-Z/ Oldest"]);
  const [sortorder, setSortorder] = useState("DESC - Z-A/ Latest");
  const [sortitem, setSortitem] = React.useState(["Title", "Created Date"]);
  const [sort, setSort] = useState("Created Date");

  //for passing search request and stores likes request related to account temporarily locally

  const handleSearchSubmit = async (e) => {

    e.preventDefault();

    const sortnum = (sortorder == "DESC - Z-A/ Latest" ? -1 : 1);

    if (alikesnumber=='') {
      alikesnumber =0
    }

    if (search.trim()) {
      dispatch(getaccountPostsBySearch({ search, filter, sort, sortnum }));
      sessionStorage.setItem("alikesr", alikesr);
      sessionStorage.setItem("alikesnumber", alikesnumber);
      navigate(`/account/search?searchQuery=${search || 'none'}`);
    } else if (search == ''){
      dispatch(getaccountPostsBySearch({ search:'', filter, sort, sortnum }));
      sessionStorage.setItem("alikesr", alikesr);
      sessionStorage.setItem("alikesnumber", alikesnumber);
      navigate(`/account/search?searchQuery=${search || 'none'}`);
    } else {
      dispatch(getaccountPosts())
      sessionStorage.setItem("alikesr", "Greater than or equal to");
      sessionStorage.setItem("alikesnumber", 0);
      navigate('/account');
    }
  };

  //for all dropdown menu

  const handlefilterChange = (e) => {
    setFilter(e.target.value);
  };

  const handlesortorderChange = (e) => {
    setSortorder(e.target.value);
  };

  const handlesortitemChange = (e) => {
    setSort(e.target.value);
  };

  const handlealikesrChange = (e) => {
    setaLikesr(e.target.value);
  };

  const styles = {
    control: base => ({
      ...base,
      fontColor: "#fffff"
    }),
    menu: base => ({
      ...base,
      fontColor: "#fffff"
    })
  };

  const handleKeyPress = (e) => {
    if (e.keyCode === 13) {
      handleSearchSubmit();
    }
  };

  return (
    <div>
      <div class="app">
        <div class="wrapper">
          <div class="left-side">
            <form autoComplete="off" noValidate onSubmit={handleSearchSubmit}>


              {/* Search */}
              <div class="side-wrapper">
                <div class="side-title">Search</div>
                <div class="side-menu">
                  <div class="search-bar">
                    <input name="search" placeholder="Search" variant="outlined" fullWidth value={search} onKeyDown={handleKeyPress} onChange={(e) => setSearch(e.target.value || '')} />
                  </div>
                </div>
              </div>

              {/* Filter */}
              <div class="side-wrapper">
                <div class="side-title">Filter based on</div>
                <div class="side-menu">
                  <a>
                    <FormControl>
                      <Select
                        classes={{
                          root: classes.whiteColor,
                          icon: classes.whiteColor
                        }}
                        disableUnderline
                        value={filter}
                        onChange={handlefilterChange}
                        inputProps={{
                          name: "filter",
                          id: "filteritem"
                        }}
                      >
                        {filteritem.map((value, index) => {
                          return <MenuItem value={value}>{value}</MenuItem>;
                        })}
                      </Select>
                    </FormControl>
                  </a>

                </div>
              </div>

              {/* Likes Range */}
              <div class="side-wrapper">
                <div class="side-title">Likes Range</div>
                <div class="side-menu">
                  <a>
                    <FormControl>
                      <Select
                        classes={{
                          root: classes.whiteColor,
                          icon: classes.whiteColor
                        }}
                        disableUnderline
                        value={alikesr}
                        onChange={handlealikesrChange}
                        inputProps={{
                          name: "alikesr",
                          id: "alikesrange"
                        }}
                      >
                        {alikesrange.map((value, index) => {
                          return <MenuItem value={value}>{value}</MenuItem>;
                        })}
                      </Select>
                    </FormControl>
                  </a>

                </div>
              </div>

              {/* Likes  number */}
              <div class="side-wrapper">
                <div class="side-title"></div>
                <div class="side-menu search-bar">
                    <input name="alikesnumber" placeholder="Likes Count" variant="outlined" fullWidth value={alikesnumber} onChange={(e) => setaLikesnumber(e.target.value || '')} />
                </div>
              </div>

              {/* Sort Item */}
              <div class="side-wrapper">
                <div class="side-title">Sort based on</div>
                <div class="side-menu">
                  <a>
                    <FormControl>
                      <Select
                        classes={{
                          root: classes.whiteColor,
                          icon: classes.whiteColor
                        }}
                        disableUnderline
                        value={sort}
                        onChange={handlesortitemChange}
                        inputProps={{
                          name: "sort",
                          id: "sortitem"
                        }}
                      >
                        {sortitem.map((value, index) => {
                          return <MenuItem value={value}>{value}</MenuItem>;
                        })}
                      </Select>
                    </FormControl>
                  </a>
                </div>
              </div>

              {/* Sort Sequence */}
              <div class="side-wrapper">
                <div class="side-title">Sort Sequence</div>
                <div class="side-menu">
                  <a>
                    <FormControl>
                      <Select
                        classes={{
                          root: classes.whiteColor,
                          icon: classes.whiteColor
                        }}
                        disableUnderline
                        value={sortorder}
                        onChange={handlesortorderChange}
                        inputProps={{
                          name: "sortorder",
                          id: "sortseq"
                        }}
                      >
                        {sortseq.map((value, index) => {
                          return <MenuItem value={value}>{value}</MenuItem>;
                        })}
                      </Select>
                    </FormControl>
                  </a>
                </div>
              </div>


              {/* Search Button */}
              <div class="side-wrapper">
                <div class="side-menu like-content">
                  <Button class="btn-secondary like-review" variant="contained" color="primary" size="large" type="submitsearch" fullWidth><strong>Submit Search</strong></Button>
                </div>
              </div>



            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;


