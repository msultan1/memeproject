import {AUTH, LOGOUT} from '../constants/actionTypes';

const authReducer = (state = {authDate:null}, action) => {
    //after user data is passed back to client side, read json file for front-end display
    switch (action.type) {
        case AUTH:
            localStorage.setItem('profile', JSON.stringify({...action?.data}));

            return {...state, authData: action?.data};
        case LOGOUT:
            localStorage.clear();
            
            return {...state, authData: null};
        default:
            return state;
    }
};

export default authReducer;