import { FETCH_ALL, FETCH_ACCOUNTALL, CREATE, UPDATE, DELETE, LIKE, COMMENT, FETCH_POST, FETCH_ACCOUNT_BY_SEARCH, FETCH_BY_SEARCH } from '../constants/actionTypes';

export default (state = { isLoading: true, posts: [] }, action) => {
  switch (action.type) {
    //for the loading button
    case 'START_LOADING':
      return { ...state, isLoading: true };
    case 'END_LOADING':
      return { ...state, isLoading: false };
    //for posts data on generic page
    case FETCH_ALL:
      return { ...state, posts: action.payload };
    //for search or filter data on generic page 
    case FETCH_BY_SEARCH:
      return { ...state, posts: action.payload.data };
    //for posts data on account page
    case FETCH_ACCOUNTALL:
      return { ...state, posts: action.payload };
    //for search or filter data on account page 
    case FETCH_ACCOUNT_BY_SEARCH:
      return { ...state, posts: action.payload.data };
    //for individual post details
    case FETCH_POST:
      return { ...state, post: action.payload };
    //for creating and updating actions of posts 
    case CREATE:
      return { ...state, posts: [...state.posts, action.payload] };
    case LIKE:
      return { ...state, posts: state.posts.map((post) => (post._id === action.payload._id ? action.payload : post)) };
    case COMMENT:
      return {
        ...state,
        posts: state.posts.map((post) => {
          if (post._id == +action.payload._id) {
            return action.payload;
          }
          return post;
        }),
      };
    case UPDATE:
      return { ...state, posts: state.posts.map((post) => (post._id === action.payload._id ? action.payload : post)) };
    case DELETE:
      return { ...state, posts: state.posts.filter((post) => post._id !== action.payload) };
    default:
      return state;
  }
};