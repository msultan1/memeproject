import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';

import postRoutes from './routes/posts.js';
import userRoutes from './routes/users.js';
import accountRoutes from './routes/account.js';

// initialize app
const app = express();
dotenv.config();


// general setup
app.use(bodyParser.json({ limit: "100mb", extended: true })) // 100mb because images can be large in size
app.use(bodyParser.urlencoded({ limit: "100mb", extended: true }))
app.use(cors()); 

app.use('/posts', postRoutes);
app.use('/user', userRoutes);
app.use('/account', accountRoutes);

//used variables from env file to save condifidential credentials

const PORT = process.env.PORT|| 5000;


// to connect to database
mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => app.listen(PORT, () => console.log(`Server Running on Port: ${PORT}`))) // if connection is successful
  .catch((error) => console.log(error.message)); // if connection is not successful
