import express from 'express';

import {getaccountPosts, updatePost, getaccountPostsBySearch, deletePost} from '../controllers/posts.js';

import auth from '../middleware/auth.js';
const router = express.Router();

//http://localhost:5000/account
router.get('/', auth, getaccountPosts);
router.get('/search', auth, getaccountPostsBySearch); //must be put in front of 'id' route to avoid routing errors
router.patch('/:id', auth, updatePost);
router.delete('/:id', auth, deletePost);

export default router; 