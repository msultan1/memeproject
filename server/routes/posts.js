import express from 'express';

import {getPosts, getPost, createPost, updatePost, likePost, commentPost, deletePost, getPostsBySearch} from '../controllers/posts.js';

import auth from '../middleware/auth.js';
const router = express.Router();

//http://localhost:5000/posts
router.get('/', getPosts);
router.get('/search', getPostsBySearch);//must be put in front of 'id' route to avoid routing errors
router.get('/:id', getPost);

router.post('/', auth, createPost); //means need to login before can do creation
router.patch('/:id', auth, updatePost);
router.delete('/:id', auth, deletePost);
router.patch('/:id/likePost', auth, likePost);
router.post('/:id/commentPost', commentPost);


export default router;