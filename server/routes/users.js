import express from 'express';

import {signin, signup} from '../controllers/users.js';

const router = express.Router();

//http://localhost:5000/user
router.post('/signin', signin); //send data from frontend to backend
router.post('/signup', signup); //send data from frontend to backend

export default router; 